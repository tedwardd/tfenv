# tfenv

*I just learned about github.com/tfutils/tfenv. This project is unaffiliated and an original rewrite. Though now I question why I should bother*

## Problem Statement

Terraform state files are particular about what version of terraform they are
modified by.  If version `A` writes to a state file, a little while later
someone running version `B` comes along and modifies it and then version `A`
wants to update the state further, it can not.

## Proposed Solution

Provide a wrapper around Terraform which catches attempts to modify state files
with a version newer than the state file was original written. If the current
version is newer, warn the user before proceeding, offering to swap to the
correct version to preserve the state file version.

## Goals

* Tool for easily updating to a new version - *COMPLETE*
* Wrapper for terraform -- *TODO*
* Tool like pyenv for easily setting and wrangling multiple projects using different TF version -- *TODO*
